# Objective

 - Student will understand rails architure and convention

 - We will go into details of working of rails applications

 - understand integration and unit testing in rails

 - build a simple rails blog
## Day 1 Rails architure
### objective 

 - understand workflow of rails

 - understand rails convention

 - build  a rails blog

 We will spend most of the time understanding, student will spend their time understanding the workflow of rails.
Student will build a simple rails blog following a guide.This will enable student to know how to quickly generate a rails app using command line  and  understand the file structure of rails application.

## Day 2 understanding active records and active storage

### objective

 - dig deep into rails models
 


## Day 3 understanding action controller, action views and routing

 - dig deeper into rails controllers

 - dig deeper into rails routes and views
  
1. controller

controller is the brain in rails application it makes sense of given request and decides where it goes to be processed and decides where processed data in display to the user.

Students will appreciate the importance of controller and understand how to manipulate requests using actions.

2. views 

user must have a plaftorm to send request and  receive response  this where view comes in.We will take look into details of creating this platform.

Student will be able to create forms, or any other mechanism to take user input and able to display to show user response via browser.

3. routing
Your request have to go somewhere or atleast it has to be direct somewhwere, well request comes inform of url and router dutifully recognize them sends them to the appropriate controller action.
We can confidently say routes are paths taken by requests and students with a little convincing will agree with me.
## Day 4  introduction to unit testing in rails 

### objective 

   - understand importance of tests

   - understand unit test when and how it used in rails applications(using rspec)
   

   Test is the quickest way to know your application works as required.We take a look why it important to test specific functionality of an application and the form building block in building bug free(or to precise application with less bugs.)

   Student will appreciate why unit test is one of the most important tests.
## Day 5 Integration Testing with Capybara and Cucumber

### Objective

 - understand what intergration testing entails

 -  understand capybara and cucumber and how to write gherkin.

 - Understand how to write user stories.

 We will go through an hybrid testing principle, Behaviour drive development.Well some argue BDD is not testing but rather software development. Am always open to debate and its my hope that after covering this you will have an informed opinion.Student will understand why BDD is important as it involves thinking like the user who is the main consumer of the application.
   





 