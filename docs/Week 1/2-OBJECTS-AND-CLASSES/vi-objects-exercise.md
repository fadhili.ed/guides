# Objects and classes exercise.

RSpec is a testing framework, for more on rspec checkout.For now do not worry much the details of rspec but know test helps ensure our code works as expected.

We have already written tests for you. Yours is to make them pass.


## install rspec
```bash
gem install rspec
```
## the Question

Create a program  which allow user to know the current population, increase population and reduce the population initial population should be **0.00**.When the user increase population by certain number then population increase by that number, if they reduce by certain number it does decrease by that number.

Methods  and variables names should be like so:

- @population_number is the population at any given instance

- current_population  

- increase_population 

- decrease_population 



Kindly provide your solution inside `population.rb` file

## Setting up environment

Create an directory population inside population directory create lib directory, inside lib create a file `population.rb`

```bash
mkdir population
cd population
mkdir lib
cd lib 
touch populaton.rb
cd ..
```




## setting up rspec

```bash
 $ cd population
 $rspec --init
 create   .rspec
 create   spec/spec_helper.rb
 $cd spec 
 $touch population_spec.rb 
 $ cd ..
```


Copy paste this code into `spec/population_spec.rb`

*spec/population_spec.rb*
```ruby
 require './lib/populaton'
    describe Account do
        it 'confirms initial population 0' do
            population = Population.new
            expect(population.population_number).to eq 0
        end       
    end
```

Before running `rspec` ensure your current working directory is `population`.


Run `rspec`. Make it pass by ensuring on instantiating new population total population show be `0`.

When test passes everything is green

Write your implemention inside the *lib/population.rb*(population.rb) file


Copy paste highlighted code into *spec/population_spec.rb* in appropriate position.

*spec/population_spec.rb*
```ruby hl_lines="7 8 9 10 "
 require './lib/populaton'
    describe Account do
        it 'confirms initial population 0' do
            population = Population.new
            expect(population.population_number).to eq 0
        end   
        it 'returns  current population' do
            population = subject.population
            expect(subject.current_population).to eq population
        end    
    end

```
Run `rspec`. Make it pass by ensuring that method current_population returns current total population.


Copy paste highlighted code into *spec/population_spec.rb* in appropriate position.

*spec/population_spec.rb*
```ruby hl_lines="11 12 13 14 "
 require './lib/populaton'
    describe Account do
        it 'confirms initial population 0' do
            population = Population.new
            expect(population.population_number).to eq 0
        end   
        it 'returns  current population' do
            population = subject.population
            expect(subject.current_population).to eq population
        end  
        it 'increases population' do
            subject.increase_population(50)
            expect(subject.current_population).to eq 50
        end        
        
    end

```

Run `rspec`. Make it pass by ensuring that method increase_population increases population by a given number.


Copy paste highlighted code into *spec/population_spec.rb* in appropriate position.

*spec/population_spec.rb*
```ruby hl_lines="15 16 17 18 19 "
 require './lib/populaton'
    describe Account do
        it 'confirms initial population 0' do
            population = Population.new
            expect(population.population_number).to eq 0
        end   
        it 'returns  current population' do
            population = subject.population
            expect(subject.current_population).to eq population
        end  
        it 'increases population' do
            subject.increase_population(50)
            expect(subject.current_population).to eq 50
        end  
        it 'decrease population' do
            subject.increase_population(50)
            subject.decrease_population(20)
            expect(subject.current_population).to eq 30
        end         
        
    end

```
Run `rspec`. Make it pass by ensuring that method decrease_population decreases population by a given number.


If everything is the you have done it.Good job





