
# Week One Objectives

- Understanding  basic syntax and convention of ruby
- understanding objects, methods and how message sending is the core
of ruby programming
- Basic introduction to test-driven development using RSpec
- Understand OOP principles such as polymorphism, abstraction, inheritance,encapsulation using ruby

### Day 1
#### Variables, Datatypes, Methods and Objects

##### Objectives

- Introduction to ruby general syntax and convention
- understanding basic ruby datatypes
- Introduction to ruby code reuse using methods
- Understanding everything in ruby is an object

  1. Variable

I will not insult you by defining what variable is. But since they form the core of the language, we will brush through them just to ensure you get the right concept.

  2. Data Types

There are lots of definitions of what programming is,  today I propose one which cuts across as the best I have ever seen(`data transformation`). As a wise man once said you must ask yourself what data type are you dealing with. The moment data type is not involved you are no longer programming. Therefore will take a moment to pay homage to programming itself. We will look at both basic and derive data type.

  3. Object

An instance of a class textbook definition. if that didn't make you laugh it proves two things either am not funny or your sense humour is below average programmer. Well, objects form the core of every OOP language because everything in OOP is an Object data type, methods, classes, modules I mean everything. This object communicates to each other by sending messages, these messages are what transform object hence manipulating data. We will take a look at objects in detail because understanding means you understand OOP.

  4. Methods

I talked about objects sending messages, well these messages are what we call methods. For instance, let's take an Object 2 and ask him or her whether she or his an integer. It goes like this `2.integer? => true`. Hey 2, are you an integer? very true I am. We will take a look at why messages are important in ruby and OOP as a whole.

  This day is one the most important days in your life to understanding ruby programming. Take basics seriously because they are the building blocks.


### Day 2

#### CONTAINER, BLOCKS, AND ITERATORS


##### Objectives

- understand and perform operations on arrays and hashes
- understand the importance and how to work with lambda and procs
- Understand the different iterators and their appropriate application.

Ruby blocks are little anonymous functions that can be passed into methods. We take a look at how these functions help in code reuse. We also discuss containers in representing a collection of data the most common containers are hashes and arrays will take a look. Some times and most of the time that is you will have a collection of data and want to manipulate each data in that collection iterators are your friends ruby has tonnes of inbuilt iterators and will spend some time understanding them.



### Day 3

#### Expression and Exception

##### Objective

- understanding control flow
- understanding types of expression
- understand exceptions

Code run top-down, left-right well this can be controlled and we can jump to top-bottom, centre bottom in sequences we want using control flow, the control flow of logic will happen more often than you think. Well what enables this to happen are expression know as controls flow, we will also look and different operator in ruby and how they help emphasize logic in programming ruby.

When you are writing code different occur among them syntax and logical..well syntax errors are brought by breaking the rules the can easily be detected.What if the error is brought by let us say dividing a number by 0, well syntax will be correct but still you will get an error. We figure out how to handle such errors using exceptions.

### Day 4
#### Inheritance: Class, Modules, Mixins
##### Objective

- Understand Method Lookup Path
- Understand access control in ruby
- understanding inheritance in class, modules and mixins

Inheritance is one of the features of OOP it allows code reuse. That is to say, you confidently make use of code in a different part of your application without having rewrite it. we will take a look at object lookup path in ruby. What you can and can't do when it comes to ruby inheritance. This concept is achieved in by classes, mixins and modules which we will take a deep dive into. We will discuss when it is appropriate to use class, module or mixin and when it is not.

### Day 5

####  UNIT TESTING AND ASSIGNMENT
##### Objectives

- test driven development using rspec
- Asseses student progress

Running the test is the only way to know whether your application works as expected in an instance. This gives test one of the paramount part of application development, this is also subject to debate among developers and by the end of this lesson you have tasted testing to an extent of making an informed decision whether to join the proponents or the opposers. We will look at the unit test which involves testing specific functionality usually of a method.
