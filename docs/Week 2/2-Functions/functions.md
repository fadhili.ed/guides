
# FUNCTIONS AND SCOPE

## FUNCTIONS

A function is a reusable block of code that groups together a sequence of statements to perform a specific task.
Function Declarations
 


A function declaration consists of:

The function keyword.

The name of the function, or its identifier, followed by parentheses.

A function body, or the block of statements required to perform a specific task, enclosed in the function’s curly brackets, { }.


```js
	function ShowMessage() {

			for(var i = 0; i < arguments.length; i++){
				console.log(arguments[i]);
			}
		}


```

Calling a Function

The code inside a function body runs, or executes, only when the function is called. 
To call a function in your code, you type the function name followed by parentheses.

`ShowMessage("Steve", "Jobs")`

### Parameters and Arguments

Parameters allow functions to accept input(s) and perform a task using the input(s). We use parameters as placeholders for information that will be passed to the function when it is called.

Default Parameters
Default parameters allow parameters to have a predetermined value in case there is no argument passed into the function.
Return
To pass back information from the function call, we use a return statement. 


### Helper Functions

These functions being called within another function are often referred to as helper functions.
Function Expressions
2nd way of defining a function

A function with no name is called an anonymous function.

### Arrow Functions

Introduced in ES6 () =>
Arrow functions remove the need to type out the keyword function
Concise Body Arrow Functions
The most condensed form of the function is known as concise body.



## SCOPE

Scope defines where variables can be accessed or referenced.
Scope is the context in which our variables are declared.
Two types:
Global Scope
Block Scope

### Blocks and Scope

A block is the code found inside a set of curly braces {}. 
Used to group one or more statements together and  to serve as a structural marker for our code.

### Global Scope
In global scope, variables are declared outside of blocks. These variables are called global variables. Because global variables are not bound inside a block, they can be accessed by any code in the program, including code in blocks.

### Block Scope

 In block scope a variable  is only accessible to the lines of code within that block.
Such variables are local variables because they are only available to the code that is in same block.

### Scope Pollution

Scope pollution is when we have too many global variables that exist in the global namespace, or when we reuse variables across different scopes. 

### Drawbacks of scope pollution

makes it difficult to keep track of our different variables and sets us up for potential accidents. 
Good scoping 
makes your code more legible since the blocks will organize your code into discrete sections.
makes your code more understandable 
Makes it easier to maintain your code, since your code will be modular
saves memory in your code because it will cease to exist after the block finishes running.

### Exercise

 Note: **kindly ensure you attempt each question in a file** rather the terminal

1. Create a file palindrome.js inside the file write a JavaScript arrow function that checks whether a passed string is palindrome or not.
A palindrome is word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses 

2. Why pay a fortune teller when you can just program your fortune yourself?
Create a file tellFortune.js inside the file
 Write a function named tellFortune that:
takes 4 arguments: number of children, partner's name, geographic location, job title.
outputs your fortune to the screen like so: "You will be a X in Y, and married to Z with N kids."
Call that function 3 times with 3 different values for the arguments.

3. Write a JavaScript arrow function that accepts a string as a parameter and converts the first letter of each word of the string in upper case e.g
'the quick brown fox' returns  'The Quick Brown Fox '

4. Create a function sumOfDifferences  that takes an array and   sum the differences between consecutive pairs in the array in descending order.
For example: `sumOfDifferences([2, 1, 10])` Returns 9
Descending order: [10, 2, 1]
Sum: (10 - 2) + (2 - 1) = 8 + 1 = 9

5. Write a JavaScript function that accepts a string as a parameter and counts the number of vowels within the string

6. Write a JavaScript  arrow function that accept a list of country names as input and returns the longest country name as out for instance Longest_Country_Name(["Australia", "Germany", "United States of America"]) returns "United States of America"

7. Return a new array consisting of elements which are multiple of their own index in input array (length > 1).



			



