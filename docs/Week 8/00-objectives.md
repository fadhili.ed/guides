# Objectives

- Understand application security in rails
- understand how to integration rails application with external apis
- understand how to debug rails application



## Day 1  and Day 2 authentication and authorizatiion and security testing


 - Understand importance of security in an application.
 - understand how to authenticate and authorize different user in application

control access by  identifying users and authorizing users to access different level of application according to their permissions level this ensure

## Day 3 and Day 4 Integrating rails with external services



- Understand how to integrate rails with github

- understand how to fetch and consume from an external API.

Build an application from ground up is sometimes an hustle and some times there are services which already existing which we want to use in our app. We learn how to integrate this existing application into our rails application using their apis.


## Day 5 Setting up and deploying production
- understand how set up different environments
- understand how to set up the application in preparation for deployment
- learn how to login to the server and run the necessary scripts

After building an application it has to be deployed. We will focus on deploying applications to production as well as managing the deploy once it has been deployed. We will also look into how to set up the application for production depending on the platform of choice.
